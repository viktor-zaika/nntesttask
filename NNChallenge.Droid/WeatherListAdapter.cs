﻿using System;
using Android.Graphics;
using Android.Views;
using Android.Widget;
using AndroidX.RecyclerView.Widget;
using FFImageLoading;
using Java.Net;
using NNChallenge.Interfaces;

namespace NNChallenge.Droid
{
    public class WeatherListAdapter : RecyclerView.Adapter
    {
        private IWeatherForcastVO source;

        public WeatherListAdapter(IWeatherForcastVO source)
        {
            this.source = source;
        }

        public override int ItemCount
        {
            get { return source.HourForecast.Length; }
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            HourViewHolder viewHolder = holder as HourViewHolder;
            var item = source.HourForecast[position];

            viewHolder.TemperatureView.Text = string.Format("{0}C / {1}F", item.TeperatureCelcius, item.TeperatureFahrenheit);
            viewHolder.DateView.Text = item.Date.ToLocalTime().ToLongTimeString();

            ImageService.Instance.LoadUrl("https:" + item.ForecastPitureURL)
               .Retry(3, 200)
               .DownSample(100, 100)
               .Into(viewHolder.Image);
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.weather_cell, parent, false);
            HourViewHolder viewHolder = new HourViewHolder(itemView);

            return viewHolder;
        }
    }

    public class HourViewHolder : RecyclerView.ViewHolder
    {
        public ImageView Image { get; set; }
        public TextView TemperatureView { get; set; }
        public TextView DateView { get; set; }

        public HourViewHolder(View itemview) : base(itemview)
        {
            Image = itemview.FindViewById<ImageView>(Resource.Id.weather_icon);
            TemperatureView = itemview.FindViewById<TextView>(Resource.Id.temperature_label);
            DateView = itemview.FindViewById<TextView>(Resource.Id.time_label);
        }
    }

}


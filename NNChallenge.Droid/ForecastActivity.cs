﻿
using System.Threading.Tasks;
using Android.App;
using Android.OS;
using AndroidX.RecyclerView.Widget;
using NNChallenge.Services;
using static AndroidX.RecyclerView.Widget.RecyclerView;

namespace NNChallenge.Droid
{
    [Activity(Label = "ForecastActivity")]
    public class ForecastActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_forecast);

            SetupListAsync();
        }

        private async Task SetupListAsync()
        {
            var service = new WeatherService();
            var city = this.Intent.GetStringExtra("city");

            var forecast = await service.GetWeatherForecast(city, 3);

            var recycleView = FindViewById<RecyclerView>(Resource.Id.weather_list);

            var layoutManager = new LinearLayoutManager(this);
            recycleView.SetLayoutManager(layoutManager);

            var adapter = new WeatherListAdapter(forecast);
            recycleView.SetAdapter(adapter);
        }
    }
}

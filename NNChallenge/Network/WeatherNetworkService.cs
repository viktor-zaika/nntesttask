﻿using System;
using System.Threading.Tasks;
using NNChallenge.NetwokrService.NetworkModels;
using NNChallenge.NetwokrService.NetworkModels.BaseNetworkModels;
using NNChallenge.Network;
using RestSharp;

namespace NNChallenge.NetwokrService
{
	public class WeatherNetworkService : IWeatherNetworkService
    {
		private BaseNetworkService networkService;

		public WeatherNetworkService()
		{
			networkService = new BaseNetworkService();
		}

		public async Task<RequestResult<WeatherResponseModel>> GetWeatherForecast(string city, int days)
		{
			var request = new RestRequest("v1/forecast.json?key=898147f83a734b7dbaa95705211612&q=" + city + "&days=" + days +"&aqi=no&alerts=no", Method.Get);
			
			var response = await networkService.ExecuteTaskAsync<WeatherResponseModel>(request);

            return response;
		}
	}
}


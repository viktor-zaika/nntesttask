﻿using System;
using NNChallenge.NetwokrService.NetworkModels;
using NNChallenge.NetwokrService.NetworkModels.BaseNetworkModels;
using System.Threading.Tasks;

namespace NNChallenge.NetwokrService
{
	public interface IWeatherNetworkService
	{
        Task<RequestResult<WeatherResponseModel>> GetWeatherForecast(string city, int days);
    }
}


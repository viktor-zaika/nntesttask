﻿using NNChallenge.NetwokrService.NetworkModels.BaseNetworkModels;
using System.Threading.Tasks;
using RestSharp;
using Plugin.Connectivity;
using Newtonsoft.Json;

namespace NNChallenge.Network
{
    public sealed class BaseNetworkService
    {
        private const string BaseUrl = @"http://api.weatherapi.com/";
        private RestClient client;

        public BaseNetworkService()
        {
            SetupClient();
        }

        private void SetupClient()
        {

            client = new RestClient(BaseUrl);
            client.AddDefaultHeader("Content-Type", "application/json");
        }

        private async Task<RequestResult<T>> ValidateNetworkResponseAsync<T>(RestResponse response, bool isLMSCall = false)
        {
            if (response.IsSuccessful)
            {
                return RequestResult<T>.Ok(JsonConvert.DeserializeObject<T>(response.Content));
            }
            else
            {
                if (response.StatusCode == 0)
                    return RequestResult<T>.NoConnection();
                else 
                    return RequestResult<T>.Failed();
            }
        }

        public async Task<RequestResult<T>> ExecuteTaskAsync<T>(RestRequest request)
        {
            try
            {
                var response = await client.ExecuteAsync(request);

                return await ValidateNetworkResponseAsync<T>(response);
            }
            catch (TaskCanceledException)
            {
                return RequestResult<T>.NoConnection(default(T));
            }
        }
    }
}


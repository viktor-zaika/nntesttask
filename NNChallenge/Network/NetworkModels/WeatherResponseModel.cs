﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace NNChallenge.NetwokrService.NetworkModels
{
    public class WeatherResponseModel
    {
        [JsonPropertyName("forecast")]
        public Forecast Forecast { get; set; }
    }

    public class Condition
    {
        [JsonPropertyName("icon")]
        public string Icon { get; set; }
    }

    public class Forecast
    {
        [JsonPropertyName("forecastday")]
        public List<Forecastday> Forecastday { get; set; }
    }

    public class Forecastday
    {
        [JsonPropertyName("hour")]
        public List<Hour> Hour { get; set; }
    }

    public class Hour
    {
        [JsonPropertyName("time_epoch")]
        public int Time_Epoch { get; set; }

        [JsonPropertyName("temp_c")]
        public float Temp_C { get; set; }

        [JsonPropertyName("temp_f")]
        public float Temp_F { get; set; }

        [JsonPropertyName("condition")]
        public Condition Condition { get; set; }
    }
}


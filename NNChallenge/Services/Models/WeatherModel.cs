﻿using System;
using NNChallenge.Interfaces;

namespace NNChallenge.Services.Models
{
	public class WeatherModel : IWeatherForcastVO
    {
		public WeatherModel()
		{
            // nothing to do
		}

        public string City { get; set; }

        public IHourWeatherForecastVO[] HourForecast { get; set; }
    }
}


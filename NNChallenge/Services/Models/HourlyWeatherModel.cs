﻿using System;
using NNChallenge.Interfaces;
using NNChallenge.NetwokrService.NetworkModels;

namespace NNChallenge.Services.Models
{
	public class HourlyWeatherModel : IHourWeatherForecastVO
    {
		public HourlyWeatherModel(Hour hour)
		{
            Date = DateTimeOffset.FromUnixTimeSeconds(hour.Time_Epoch).DateTime;
            TeperatureCelcius = hour.Temp_C;
            TeperatureFahrenheit = hour.Temp_F;
            ForecastPitureURL = hour.Condition.Icon;
        }

        public DateTime Date { get; set; }

        public float TeperatureCelcius { get; set; }

        public float TeperatureFahrenheit { get; set; }

        public string ForecastPitureURL { get; set; }
    }
}


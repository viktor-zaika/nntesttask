﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NNChallenge.Interfaces;
using NNChallenge.NetwokrService;
using NNChallenge.Services.Models;

namespace NNChallenge.Services
{
	public class WeatherService
	{
		private WeatherNetworkService networkService;
		public WeatherService()
		{
			networkService = new WeatherNetworkService();
		}

		public async Task<IWeatherForcastVO> GetWeatherForecast(string city, int days)
		{
			var result = await networkService.GetWeatherForecast(city, days);


            if (result.Successfull)
			{
				var forecastHours = result.Result.Forecast.Forecastday.SelectMany(x => x.Hour).ToList();

				return new WeatherModel()
				{
					City = city,
					HourForecast = forecastHours.Select(t => new HourlyWeatherModel(t)).ToArray()
				};
			}

			return null;
		}
	}
}


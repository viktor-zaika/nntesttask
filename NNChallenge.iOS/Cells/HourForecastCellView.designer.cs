// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace NNChallenge.iOS
{
	[Register ("HourForecastCellView")]
	partial class HourForecastCellView
	{
		[Outlet]
		UIKit.UILabel TemperatureLabel { get; set; }

		[Outlet]
		UIKit.UILabel TimeLabel { get; set; }

		[Outlet]
		UIKit.UIImageView WeatherImage { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (WeatherImage != null) {
				WeatherImage.Dispose ();
				WeatherImage = null;
			}

			if (TemperatureLabel != null) {
				TemperatureLabel.Dispose ();
				TemperatureLabel = null;
			}

			if (TimeLabel != null) {
				TimeLabel.Dispose ();
				TimeLabel = null;
			}
		}
	}
}

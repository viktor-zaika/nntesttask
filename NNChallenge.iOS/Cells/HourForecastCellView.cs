﻿using System;
using FFImageLoading;
using Foundation;
using NNChallenge.Interfaces;
using UIKit;

namespace NNChallenge.iOS
{
	public partial class HourForecastCellView : UITableViewCell
	{
		public static readonly NSString Key = new NSString ("HourForecastCellView");
		public static readonly UINib Nib;

		static HourForecastCellView ()
		{
			Nib = UINib.FromName (Key, NSBundle.MainBundle);
		}

		protected HourForecastCellView (IntPtr handle) : base (handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

        public void UpdateData(IHourWeatherForecastVO hourForecast)
        {
            TemperatureLabel.Text = string.Format("{0}C / {1}F", hourForecast.TeperatureCelcius, hourForecast.TeperatureFahrenheit );
            TimeLabel.Text = hourForecast.Date.ToLocalTime().ToLongTimeString();

			// Delayed images downloading
            ImageService.Instance.LoadUrl("https:" + hourForecast.ForecastPitureURL)
               .Retry(3, 200)
               .DownSample(100, 100)
               .Into(ImageView);
        }
    }
}

﻿using System;
using Foundation;
using NNChallenge.Interfaces;
using UIKit;

namespace NNChallenge.iOS
{
	public class WeatherTableSource : UITableViewSource
    {
        private readonly IWeatherForcastVO WeatherForecast;

        public WeatherTableSource(IWeatherForcastVO weatherForcast)
        {
            WeatherForecast = weatherForcast;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return WeatherForecast.HourForecast.Length;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            HourForecastCellView cell = tableView.DequeueReusableCell(HourForecastCellView.Key) as HourForecastCellView;

            cell.UpdateData(WeatherForecast.HourForecast[indexPath.Row]);

            return cell;
        }
    }
}


﻿using System;
using NNChallenge.Interfaces;
using NNChallenge.Services;
using UIKit;

namespace NNChallenge.iOS
{
    public partial class ForecastViewController : UIViewController
    {
        public IWeatherForcastVO WeatherForecast;
        public ForecastViewController() : base("ForecastViewController", null)
        {
            // Nothing to do
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            Title = "Forecast";
            
            WeatherTable.RegisterNibForCellReuse(HourForecastCellView.Nib, HourForecastCellView.Key);
            WeatherTable.Source = new WeatherTableSource(WeatherForecast);
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}

